import { Request, Response, NextFunction } from 'express';

export const parserErrorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
    if (err) {
        console.error("Invalid JSON");
        return res.status(400).send({
            error: "Invalid JSON",
            details: null
        })
    }
    next();
}