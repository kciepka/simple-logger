import { Response } from 'express';

export interface QueryResultValidatorOptions {
    expectData: boolean
}

export const queryResultValidator = (res: Response, handler: (data: any) => any,
    options?: QueryResultValidatorOptions) => {
    const opts = Object.assign({}, { expectData: true }, options);
    return (err: Error, data?: any) => {
        if (err) {
            console.error(err);
            return res.status(500).send({
                error: "Could not get entity",
                details: err
            });
        }

        if (opts.expectData && !data) {
            return res.status(404).send({
                error: "Entity not found",
                details: null
            });
        }
        return handler(data);
    }
}