import * as dns from 'dns';

export class DomainVerifier {
    constructor() { }

    public static verify(domain: string, verificationCode: string) {
        return new Promise((resolve, reject) => {
            dns.resolveTxt(domain, (err: Error, addresses: string[][]) => {
                if (err) {
                    return reject(err);
                }

                if (!addresses || !addresses.length) {
                    return reject("No TXT records found for domain");
                }

                let verified = false;
                addresses.forEach(address => {
                    if (address[0] === verificationCode) {
                        verified = true;
                    }
                })
                verified ? resolve(true) : reject("Domain verification code not found in DNS table");
            });
        });
    }
}
