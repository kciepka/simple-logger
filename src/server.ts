import express from 'express';
import bodyParser from 'body-parser';
import passport = require('passport');
import Api from './routes/Api';
import Auth from './routes/Auth';
import { initPassport } from './auth/passport.config';
import { IConfig } from './types/IConfig';
import { initMongoose } from './db/mongoose.config';
import { errors } from 'celebrate';
import { parserErrorHandler } from './middleware/parser-error-handler';
import { Mailer } from './mailer';
import cors from 'cors';

const config: IConfig = require('../config.json');
const app: express.Express = express();

const mailer = new Mailer({
    host: config.mail.host,
    port: config.mail.port,
    ssl: config.mail.ssl,
    username: config.mail.username,
    password: config.mail.password
});

app.set('mailer', mailer);
app.set('config', config);

initMongoose(config);
initPassport(config);

app.use(cors());
app.use(bodyParser.json());
app.use(parserErrorHandler);

app.use(passport.initialize());

app.use('/auth', Auth);
app.use('/api', Api);

app.use(errors());

app.listen(config.express.port, () => {
    console.log(`Server listening on port ${config.express.port}`);
});
