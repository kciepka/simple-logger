
import mongoose from 'mongoose';
import { IConfig } from '../types/IConfig';
import { LogCleaner } from '../log-cleaner';

export const initMongoose = (config: IConfig) => {
    mongoose.connect(
        `mongodb://${config.mongo.host}:${config.mongo.port}/${config.mongo.dbName}`,
        { useNewUrlParser: true, useCreateIndex: true }
    );
    const db = mongoose.connection;

    db.on('error', (err) => {
        console.error('Mongoose error: ', err)
    });

    db.once('open', () => {
        const logCleaner = new LogCleaner();
        console.log("Database connection open");
    });
}
