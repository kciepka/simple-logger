export interface IGoogleProfile {
    id: string
    displayName: string
    name: {
        familyName: string,
        givenName: string
    },
    gender: string,
    emails: Array<{ value: string, type: string }>
    provider: string,
    photos: [{
        value: string
    }],
    _raw: string,
    _json: {
        id: string
        etag: string,
        objectType: string,
        displayName: string,
        url: string,
        isPlusUser: boolean,
        verified: boolean,
        kind: string,
        circledByCount: number,
        name: {
            familyName: string,
            givenName: string
        },
        gender: string,
        image: [{
            url: string
        }]
    }
}