export type IConfig = {
    auth: {
        jwt: {
            secret: string,
            expiresIn: string | number,
            issuer: string
        },
        google: {
            clientID: string,
            secret: string,
            callbackUrl: string
        }
    }
    express: {
        port: number
    },
    mail: {
        host: string,
        port: number,
        ssl: boolean,
        from: string,
        username: string,
        password: string,
        templates: {
            logNotififaction: {
                subject: string,
                text: string
            }
        }
    }
    mongo: {
        host: string,
        port: number,
        dbName: string
    }
}