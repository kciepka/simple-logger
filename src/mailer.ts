import nodemailer, { Transporter } from 'nodemailer';

export class Mailer {

    private transport: Transporter;

    constructor(config: MailerConfig) {
        this.transport = nodemailer.createTransport({
            host: config.host,
            port: config.port,
            secure: config.ssl,
            auth: {
                user: config.username,
                pass: config.password
            }
        });
    }

    sendMail(options: MailOptions): Promise<SentMessageInfo> {
        return this.transport.sendMail(options);
    }
}

export interface MailerConfig {
    host: string,
    port: number,
    ssl: boolean,
    username: string,
    password: string
}

export interface MailOptions {
    from: string,
    to: string,
    subject?: string,
    text?: string,
    html?: string
}

export interface SentMessageInfo {
    accepted: Array<string>,
    rejected: Array<string>,
    envelopeTime: number,
    messageTime: number,
    messageSize: number,
    response: string,
    envelope: {
        from: string,
        to: Array<string>
    },
    messageId: string
}