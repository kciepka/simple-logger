import { Schema, Document, model } from "mongoose";

const logSchema = new Schema({
    id: { type: String, unique: true, required: true },
    level: {
        type: String,
        enum: [
            'VERBOSE',
            'DEBUG',
            'INFO',
            'WARNING',
            'ERROR',
            'CRITICAL'
        ],
        required: true
    },
    appId: { type: String, required: true },
    value: { type: String, required: true },
    timestamp: { type: Date, default: Date.now, required: true },
}, { collection: 'logs', timestamps: true, versionKey: false });

logSchema.methods.toJSON = function () {
    const obj = this.toObject();
    delete obj._id;
    return obj;
}

export interface ILog extends Document {
    id: string,
    level: string,
    appId: string,
    value: String,
    createdAt: Date,
    updatedAt: Date,
    timestamp: Date
}

const Log = model("Log", logSchema);
export default Log;