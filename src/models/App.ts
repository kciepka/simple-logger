import { Schema, Document, model } from "mongoose";

const appSchema = new Schema({
    id: { type: String, unique: true, required: true },
    name: { type: String, default: 'my app', required: true },
    ownerId: { type: String, required: true },
    origin: { type: String, lowercase: true, validate: /^[0-9a-zA-Z.]+$/ },
    originVerified: { type: Boolean, default: false },
    originVerificationCode: String,
    apiKey: String,
    oldApiKeys: { type: [String], default: [] },
    settings: {
        notify: { type: Boolean, default: false },
        notificationLogLevel: { type: String, default: 'CRITICAL' },
        storageTime: { type: Number, default: 30 }
    }
}, { collection: 'apps', timestamps: true, versionKey: false });

appSchema.methods.toJSON = function () {
    const obj = this.toObject();
    delete obj._id;
    return obj;
}

export interface IApp extends Document {
    id: string,
    name: string,
    ownerId: string,
    origin: string,
    originVerified: boolean,
    originVerificationCode: string,
    apiKey: string | null,
    oldApiKeys: string[],
    createdAt: Date,
    updatedAt: Date,
    settings: {
        notificationLogLevel: string,
        notify: boolean,
        storageTime: number
    }
}

const App = model<IApp>("App", appSchema);
export default App;