import { Schema, Document, model } from "mongoose";
import { Guid } from "guid-typescript";
import { IApp } from "./App";

const userSchema = new Schema({
    id: { type: String, unique: true },
    firstName: { type: String, default: "Anonymous" },
    lastName: String,
    avatarUrl: String,
    email: String,
    gender: String,
    displayName: String,
    googleId: { type: String, unique: true }
}, { collection: 'users', timestamps: true, versionKey: false });

userSchema.methods.toJSON = function () {
    const obj = this.toObject();
    delete obj._id;
    return obj;
}

userSchema.methods.findOrCreate = function (query: any, cb: any) {
    const self = this;
    User.findOne(query, (err: Error, user: IUser) => {
        if (err) {
            cb(err, null);
        }

        if (!user) {
            const user = new User({
                id: Guid.create(),
                displayName: self.displayName,
                firstName: self.firstName,
                lastName: self.lastName,
                email: self.email,
                avatarUrl: self.avatarUrl,
                gender: self.gender,
                googleId: self.googleId
            });

            return user.save()
                .then((user) => {
                    cb(null, user);
                })
                .catch(err => {
                    cb(err, null);
                });
        }
        cb(null, user);
    })
}

export interface IUser extends Document {
    id: string,
    firstName: string,
    lastName: string,
    displayName: string,
    avatarUrl: string,
    gender: string,
    email: string,
    createdAt: Date,
    updatedAt: Date,
    apps: Array<IApp>
    findOrCreate: (query: any, cb: any) => IUser
}

const User = model<IUser>("User", userSchema);
export default User;