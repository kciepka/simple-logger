import passport from 'passport';
import { OAuth2Strategy } from 'passport-google-oauth'
import { IConfig } from '../types/IConfig';
import * as jwt from 'jsonwebtoken';
import User, { IUser } from '../models/User';
import { IGoogleProfile } from '../types/IGoogleProfile';

export const initPassport = (config: IConfig) => {
    function createToken(accessToken: string, refreshToken: string, profile: IGoogleProfile,
        done: (error: any, user?: any) => void) {

        const user = new User({
            googleId: profile.id,
            email: profile.emails && profile.emails[0] && profile.emails[0].value || null,
            firstName: profile.name.givenName,
            lastName: profile.name.familyName,
            gender: profile.gender,
            displayName: profile.displayName,
            avatarUrl: profile.photos && profile.photos[0] && profile.photos[0].value || null
        });

        user.findOrCreate({ googleId: profile.id }, (err: Error, user: IUser) => {
            if (err) {
                console.error(err);
                done(err, null);
            }
            jwt.sign({}, config.auth.jwt.secret,
                {
                    expiresIn: config.auth.jwt.expiresIn,
                    header: {},
                    subject: user.id,
                    issuer: config.auth.jwt.issuer
                },
                (err, token) => {
                    done(err, { ...user, token });
                });
        });
    }

    passport.use(new OAuth2Strategy(
        <any>{
            clientID: config.auth.google.clientID,
            clientSecret: config.auth.google.secret,
            callbackURL: config.auth.google.callbackUrl
        },
        <any>createToken
    ));

    passport.serializeUser(function (user, cb) {
        cb(null, user);
    });

    passport.deserializeUser(function (obj, cb) {
        cb(null, obj);
    });

    return passport;
} 
