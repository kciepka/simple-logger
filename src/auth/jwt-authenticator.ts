import { Request, Response, NextFunction } from 'express';
import User, { IUser } from '../models/User';
import * as jwt from 'jsonwebtoken';
import { IConfig } from '../types/IConfig';

export const JwtAuthenticator = (req: Request, res: Response, next: NextFunction) => {
    const token: string | undefined = req.headers['authorization'];

    if (!token) {
        console.error('Authorization header not found');
        return res.sendStatus(401);
    }

    const normalizedToken = token.substring(7, token.length);
    const config: IConfig = req.app.get('config');

    jwt.verify(normalizedToken, config.auth.jwt.secret,
        {
            clockTolerance: 60,
            issuer: config.auth.jwt.issuer
        },
        (err, decoded) => {
            if (err) {
                console.error(err);
                return res.sendStatus(401);
            }

            const id = (<any>decoded).sub;

            User.findOne({ id }, (err: Error, user: IUser) => {
                if (err) {
                    console.error(err);
                    return res.sendStatus(401);
                }

                if (!user) {
                    console.error("User not found");
                    return res.sendStatus(401);
                }
                req.user = user;
                next();
            });
        });
}