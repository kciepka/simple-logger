import * as cron from 'cron';
import App, { IApp } from './models/App';
import Log from './models/Log';

export class LogCleaner {
    constructor() {
        new cron.CronJob('0 3 * * *', () => {
            App.find((err: Error, apps: IApp[]) => {
                if (err) {
                    console.error(err);
                    return;
                }
                apps.forEach(app => {
                    const maxOld = new Date(new Date().getTime() - app.settings.storageTime * 24 * 60 * 60 * 1000);
                    Log.deleteMany({ appId: app.id, createdAt: { $lte: maxOld } }, (err: Error) => {
                        if (err) {
                            console.error(err);
                        }
                        console.log(`Successfully removed logs for ${app.id} created before ${maxOld}`);
                    });
                });
            });
        }, undefined, true);
    }
}