import passport from 'passport';
import express from 'express';
import { celebrate, Joi } from 'celebrate';

const router = express.Router();

router.get('/google/login',
    (req, res, next) => {
        const state = req.query.redirect_uri;
        passport.authenticate('google', { scope: ["openid", "email"], state })(req, res, next);
    }
);

router.get('/google/callback', passport.authenticate('google', { scope: ["openid", "email"] }),
    celebrate({
        query: Joi.object().keys({
            state: Joi.string(),
            code: Joi.string(),
            scope: Joi.string(),
            authuser: Joi.string(),
            session_state: Joi.string(),
            prompt: Joi.string()
        })
    }),
    (req, res) => {
        if (!req.query.state) return res.status(400).send("No redirect_uri provided");
        res.redirect(`${req.query.state}?access_token=${req.user.token}`);
    }
);

export default router;