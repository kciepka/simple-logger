import express from 'express';
import crypto from 'crypto';
import { JwtAuthenticator } from '../auth/jwt-authenticator';
import App, { IApp } from '../models/App';
import { celebrate, Joi } from 'celebrate';
import { Guid } from 'guid-typescript';
import Log, { ILog } from '../models/Log';
import { DomainVerifier } from '../domain-verifier';
import { queryResultValidator } from '../middleware/query-result-validator';
import { SentMessageInfo } from '../mailer';
import { IConfig } from '../types/IConfig';

const router = express.Router();

router.get('/applications', JwtAuthenticator,
    celebrate({
        query: Joi.object().keys({
            offset: Joi.number().default(0),
            limit: Joi.number().default(20),
            sort: Joi.string().default('desc'),
            sortBy: Joi.string().default('createdAt')
        })
    }),
    (req, res) => {
        App.find({ ownerId: req.user.id })
            .skip(req.query.offset)
            .limit(req.query.limit)
            .sort({ [req.query.sortBy]: req.query.sort })
            .exec(queryResultValidator(res, (apps: IApp[]) => {
                res.send(apps);
            }));
    }
)

router.post('/applications', JwtAuthenticator,
    celebrate({
        body: Joi.object().keys({
            name: Joi.string(),
            origin: Joi.string().regex(/^[0-9a-zA-Z.]+$/).required()
        })
    }),
    (req, res) => {
        const app = new App({
            id: Guid.create().toString(),
            ownerId: req.user.id,
            name: req.body.name,
            origin: req.body.origin,
            originVerified: false,
            originVerificationCode: crypto.randomBytes(20).toString('hex')
        });

        app.save()
            .then((app: IApp) => {
                res.status(201).send(app);
            })
            .catch((err: Error) => {
                console.error(err);
                res.status(500).send({
                    error: "Could not save entity",
                    details: err
                });
            });
    });

router.put('/applications/:id', JwtAuthenticator,
    celebrate({
        body: Joi.object().keys({
            name: Joi.string(),
            id: Joi.string(),
            origin: Joi.string(),
            oldApiKeys: Joi.array().items(Joi.string()),
            originVerified: Joi.boolean(),
            ownerId: Joi.string(),
            createdAt: Joi.date(),
            updatedAt: Joi.date(),
            originVerificationCode: Joi.string(),
            settings: Joi.object().keys({
                notify: Joi.boolean().default(false),
                notificationLogLevel: Joi.string().default('CRITICAL'),
                storageTime: Joi.number().default(30)
            })
        }).options({ stripUnknown: true })
    }),
    (req, res) => {
        App.findOne({ id: req.params.id, ownerId: req.user.id }, queryResultValidator(res, (app: IApp) => {
            app.name = req.body.name;
            app.originVerified = (app.origin === req.body.origin);
            app.origin = req.body.origin;
            if (!app.originVerified && app.apiKey) {
                app.oldApiKeys = [...app.oldApiKeys, app.apiKey];
                app.apiKey = null;
            }
            app.settings = req.body.settings;

            app.save()
                .then(() => {
                    res.send(app);
                })
                .catch(err => {
                    res.status(500).send({
                        error: "Could not update app",
                        details: err
                    });
                });
        }));
    });

router.delete('/applications/:id', JwtAuthenticator, (req, res) => {
    App.deleteOne({ id: req.params.id, ownerId: req.user.id }, queryResultValidator(res, () => {
        Log.deleteMany({ appId: req.params.id }, queryResultValidator(res, () => {
            res.sendStatus(204);
        }, { expectData: false }));
    }, { expectData: false }));
});

router.post('/applications/:id/apikey', JwtAuthenticator, (req, res) => {
    App.findOne({ id: req.params.id, ownerId: req.user.id }, queryResultValidator(res, (app: IApp) => {
        if (!app.originVerified) {
            return res.status(403).send({
                error: "Origin not verified",
                details: null
            });
        }

        if (app.apiKey) {
            app.oldApiKeys = [...app.oldApiKeys, app.apiKey];
        }

        app.apiKey = Guid.create().toString();

        app.save()
            .then(() => {
                res.status(201).send(app);
            })
            .catch(err => {
                console.error(err);
                res.status(500).send({
                    error: "Could not save app",
                    details: err
                });
            });
    }));
});

router.post('/applications/:id/verification', JwtAuthenticator, (req, res) => {
    App.findOne({ id: req.params.id, ownerId: req.user.id }, queryResultValidator(res, (app: IApp) => {
        DomainVerifier.verify(app.origin, app.originVerificationCode)
            .then(() => {
                app.originVerified = true;
                app.save()
                    .then(() => {
                        res.status(201).send(app);
                    })
                    .catch(err => {
                        console.error(err);
                        res.status(500).send({
                            error: "Could not save app",
                            details: err
                        });
                    });
            })
            .catch(err => {
                console.error(err);
                res.status(400).send({
                    error: `Could not verify domain ${app.origin}`,
                    details: err
                });
            });
    }));
});

router.get('/logs/recent', JwtAuthenticator,
    celebrate({
        query: Joi.object().keys({
            limit: Joi.number().default(20),
            sort: Joi.string().default('desc'),
            sortBy: Joi.string().default('createdAt')
        })
    }), (req, res) => {
        App.find({ ownerId: req.user.id }, (err, apps: IApp[]) => {
            if (err) {
                return res.sendStatus(500);
            }

            if (!apps) return res.send([]);

            const promises: Array<Promise<ILog[]>> = [];
            let allLogs: ILog[] = [];

            apps.forEach(app => {
                promises.push(new Promise((resolve, reject) => {
                    Log.find({ appId: app.id }, (err, logs: ILog[]) => {
                        if (err) {
                            return reject(err);
                        }
                        allLogs = allLogs.concat(logs);
                        resolve(logs);
                    });
                }));
            });

            Promise.all(promises)
                .then(() => {
                    res.send(allLogs.sort((a, b) => {
                        return b.timestamp.getTime() - a.timestamp.getTime();
                    }).slice(0, req.query.limit));
                }).catch(err => {
                    res.sendStatus(500);
                });
        })
            .sort({ [req.query.sortBy]: req.query.sort })
            .limit(req.query.limit);
    });

router.get('/logs', JwtAuthenticator,
    celebrate({
        query: Joi.object().keys({
            apiKey: Joi.string().required(),
            offset: Joi.number().default(0),
            limit: Joi.number().default(20),
            sort: Joi.string().default('desc'),
            sortBy: Joi.string().default('createdAt'),
            from: Joi.date(),
            to: Joi.date()
        })
    }),
    (req, res) => {
        App.findOne({
            apiKey: req.query.apiKey
        }, queryResultValidator(res, (app: IApp) => {
            const query = {
                appId: app.id,
                createdAt: {
                    $gte: req.query.from ? new Date(req.query.from) : new Date(0),
                    $lte: req.query.to ? new Date(req.query.to) : new Date()
                }
            }
            Log.countDocuments(query).exec((err: Error, count: number) => {
                if (err) {
                    console.error(err);
                    return res.status(500).send({
                        error: "Could not get logs count",
                        details: err
                    });
                }
                Log.find(query)
                    .sort({ [req.query.sortBy]: req.query.sort })
                    .skip(req.query.offset)
                    .limit(req.query.limit)
                    .exec((err: Error, logs: ILog[]) => {
                        if (err) {
                            console.error(err);
                            return res.status(500).send({
                                error: "Could not get logs",
                                details: err
                            });
                        }
                        res.send({ result: logs, pagination: { offset: req.query.offset, total: count } });
                    });
            })
        }));
    }
)

router.post('/logs', JwtAuthenticator,
    celebrate({
        body: Joi.object().keys({
            value: Joi.string().required(),
            timestamp: Joi.date(),
            level: Joi.string().required(),
            apiKey: Joi.string().required()
        })
    }),
    (req, res) => {
        App.findOne({ apiKey: req.body.apiKey }, queryResultValidator(res, (app: IApp) => {
            const log = new Log({
                id: Guid.create().toString(),
                appId: app.id,
                value: req.body.value,
                timestamp: req.body.timestamp,
                level: req.body.level
            });

            log.save()
                .then((resp) => {
                    if (app.settings.notify && req.body.level === app.settings.notificationLogLevel) {
                        const config: IConfig = req.app.get('config');
                        req.app.get('mailer').sendMail({
                            from: config.mail.from,
                            to: req.user.email,
                            subject: config.mail.templates.logNotififaction.subject,
                            text: config.mail.templates.logNotififaction.text
                        })
                            .then((info: SentMessageInfo) => {
                                console.log("Notification e-mail sent: ", info.messageId);
                            })
                            .catch((err: Error) => console.error(err));
                    }
                    res.status(201).send(resp);
                })
                .catch((err) => {
                    console.error(err);
                    res.status(500).send({
                        error: "Could not save log",
                        details: err
                    });
                });
        }));
    });

router.delete('/logs/:id', JwtAuthenticator, (req, res) => {
    Log.findOne({ id: req.params.id }, queryResultValidator(res, (log: ILog) => {
        App.findOne({ id: log.appId }, queryResultValidator(res, (app: IApp) => {
            if (app.ownerId !== req.user.id) {
                return res.status(403).send({
                    error: "Permission denied",
                    details: null
                });
            }
            Log.deleteOne({ id: req.params.id }, queryResultValidator(res, () => {
                res.sendStatus(204);
            }));
        }));
    }));
});

router.delete('/applications/:id/logs', JwtAuthenticator, (req, res) => {
    App.findOne({ id: req.params.id }, queryResultValidator(res, (app: IApp) => {
        if (app.ownerId !== req.params.id) {
            return res.status(403).send({
                error: "Permission denied",
                details: null
            });
        }
        Log.deleteMany({ appId: req.params.id }, queryResultValidator(res, () => {
            res.sendStatus(204);
        }, { expectData: false }));
    }));
});

export default router;