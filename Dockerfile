FROM node:10

WORKDIR /app

COPY package*.json ./

COPY tsconfig.json ./

COPY config.json ./

RUN npm install

COPY ./src ./src

RUN npm run build

EXPOSE 80

CMD [ "npm", "start" ]
